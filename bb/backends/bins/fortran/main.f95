program main
! https://cyber.dabamos.de/programming/modernfortran/sqlite.html

    use, intrinsic :: iso_c_binding, only: c_ptr
    use :: sqlite
    implicit none
    character(len=*), parameter :: DB_FILE  = "/opt/bb/bb.db"
    character(len=*), parameter :: DB_TABLE = 'posts'

    character(len=:), allocatable :: errmsg ! Error message.
    integer                       :: rc     ! Return code.
    type(c_ptr)                   :: db     ! SQLite database.
    type(c_ptr)                   :: stmt   ! SQLite statement.
    type(c_ptr)                   :: udp    ! User-data pointer.


    print *, "<!DOCTYPE html>"
    print *, "<html><head><link rel='stylesheet' href='/assets/css/skeleton.css'> &
             <link rel='stylesheet' href='/assets/css/nixphere.css'> &
             <link rel='stylesheet' href='/assets/css/normalize.css'> &
             <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> &
             <title>FORTRAN bb</title> </head> <body> <div class='container'><div class='row'> &
             <div class='twelve columns'><h1>A Modern FORTRAN backend. </h1> &
             <hr></div><div class='twelve columns post'>"

    ! Open db
    rc = error(sqlite3_open(DB_FILE, db), 'sqlite3_open()')
    if (rc /= SQLITE_OK) stop


    rc = error(sqlite3_prepare(db, "select * from posts order by id desc", stmt), 'sqlite3_prepare()')

    do while (sqlite3_step(stmt) /= SQLITE_DONE)
        print *, "<h5 class='post'>"
        call printer(stmt, 0)
        call printer(stmt, 1)
        call printer(stmt, 2)
        print *, "</h5>"
        print *, "<p>"
        call printer(stmt, 3)
        print *, "</p>"
    end do


    rc = error(sqlite3_finalize(stmt), 'sqlite3_finalize()')
    rc = error(sqlite3_close(db), 'sqlite3_close()')

    print *, "</html>"


contains
        function error(code,str)
                !! print err msg
                integer, intent(in) :: code
                character(len=*), intent(in), optional :: str
                integer :: error

                error = code

                if (code == SQLITE_OK .or. code == SQLITE_DONE) return

                if (present(str)) then
                        print *, "<!--"
                        print '(a, ":failed")', str
                        print *, "-->"
                        return
                end if

                print '("Unknown error")'
        end function error

        subroutine printer(stmt, col)
                type(c_ptr), intent(inout) :: stmt
                integer,     intent(in)    :: col
                integer                    :: col_type
                integer                    :: i
        
                !! no need for loop
                !do i = 0, col - 1
                    i = col !! cope with no loop
                    col_type = sqlite3_column_type(stmt, col)
        
                    select case (col_type)
                        case (sqlite_integer)
                            write (*, '(i12)', advance='no') sqlite3_column_int(stmt, i)
                            !print *,sqlite3_column_int(stmt, i)
        
                        case (sqlite_float)
                            write (*, '(f0.8)', advance='no') sqlite3_column_double(stmt, i)
        
                        case (sqlite_text)
                            write (*, '(a, " ")', advance='no') sqlite3_column_text(stmt, i)
        
                        case default
                            write (*, '(" unsupported")', advance='no')
                    end select

                !end do
                print *
            end subroutine printer

end program main
