       IDENTIFICATION DIVISION.
       PROGRAM-ID. BB.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
           FILE-CONTROL.
               SELECT INPUT-FILE ASSIGN TO '/opt/bb/dump.csv'
               ORGANIZATION IS LINE SEQUENTIAL
               ACCESS MODE IS SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  INPUT-FILE          RECORD CONTAINS 900 CHARACTERS.
       01  INPUT-RECORD        PIC X(900).

       WORKING-STORAGE SECTION.

       01  SEPARATE-POST.
           05 ID-POST          PIC 9(3).
           05 USERNAME         PIC X(30).
           05 ORARIO           PIC X(40).
           05 CONTENUTO        PIC X(800).
       
       01 WS-EOF PIC A(1).
           

           
       

       PROCEDURE DIVISION.

           DISPLAY '<!DOCTYPE html>'.
    
    
           
           DISPLAY '<html lang="en"><head>'.
           DISPLAY '<link rel="stylesheet" href="/assets/css/skeleton.css"><link rel="stylesheet" href="/assets/css/nixphere.css">'.
           DISPLAY '<link rel="stylesheet" href="/assets/css/normalize.css"><link rel="stylesheet" href="/assets/forkawesome/css/fork-awesome.min.css">'.
           DISPLAY '<meta charset="utf-8"/><title>Very modern COBOL backend</title></head>'.
           
           DISPLAY '<body>'


           DISPLAY '<div class="container">'.
           DISPLAY '<div class="row">'.

	   DISPLAY '<div class="twelve columns">'.
           DISPLAY '<h1>A very modern <strong>COBOL</strong> backend</h1>'.
	   DISPLAY '<hr>'
           DISPLAY '</div>'
           OPEN INPUT INPUT-FILE.
               PERFORM UNTIL WS-EOF='Y'
                   MOVE SPACES TO INPUT-RECORD
                   MOVE SPACES TO SEPARATE-POST
                   READ INPUT-FILE NEXT RECORD INTO INPUT-RECORD
                       AT END MOVE 'Y' TO WS-EOF
                       NOT AT END UNSTRING INPUT-RECORD DELIMITED BY "|"
                       INTO ID-POST, USERNAME, ORARIO, CONTENUTO
                       DISPLAY '<article class="twelve columns">'
                       DISPLAY '<header><h5>#'ID-POST' written by 'USERNAME' on 'ORARIO'</h5></header>'
                        '<section><p>'CONTENUTO'</section></p>'
                       DISPLAY '</article>'
                   END-READ

           
                   

               END-PERFORM
           DISPLAY '</div></div>'.

           DISPLAY '</body>'.
	   DISPLAY '</html>'.
           CLOSE INPUT-FILE.
           STOP RUN.


           

           
