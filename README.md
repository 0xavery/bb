# httpd setup

```
cp -rv bb/* /usr/local/www/apache24/data/bb
```

# maintainabilty

no

# build script

maybe

# install dependencies

do not run

```
pkg search -g \* | cut -d " " -f 1 | xargs pkg install -f
```


# Credits, Licensing
## Dependencies
* [normalize.css](https://github.com/necolas/normalize.css/) MIT
* [skeleton.css](https://github.com/dhg/Skeleton) MIT

## User Contributed
Asusme PHP wrapper scripts (ex Rust.php, Lua.php) are written by the same author that wrote the actual backend. Unless otherwise specified, all software is licensed under the GNU AGPL-3.0 license.

### Avery 
* Core Backend
	- apache-getposts.sh
	- getposts.sh
	- mkdb.sh
	- mktables.sql
	- newpost.sh
	- viewposts.sh
	- bb/engines.php
	- bb/index.php

* Additional Backends
	- C++
	- C
	- Lua
	- PHP
	- POSIX\_Shell
	- R
	- Ruby
	- FORTRAN
	- Perl
	- TCL

### Celeste 
* Core Backend
	- updatecsv.sh
* Backends
	- COBOL
	- Python
	- Rust
