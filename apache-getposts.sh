#!/bin/sh

posts=$(echo "select count('1') from posts" | sqlite3 /opt/bb/bb.db)
echo "<!DOCTYPE html>"
echo "<head> <link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <style> .title{ text-decoration: underline; } .name{ text-transform: capitalize; } .content{ max-width: 75%; } .post{ margin: 1%; padding: 1%; border-radius: 10px; } */ </style> <title>bb</title> </head> <body> <div class='container'><div class='row'><div class='twelve columns'><h1>bb: a bulletin board system written in many languages</h1><p> This backend is written in <b>POSIX shell</b> (mostly awk)</p><p> Total Posts: ${posts} </p><hr></div><div class='twelve columns post'>"

head=$(echo "SELECT * FROM posts ORDER BY id DESC;" | sqlite3 /opt/bb/bb.db )

echo "${head}" | awk -F \| '{print "<h5 class=\"title\">#" $1 " Posted by <b>" $2 "</b> on " $3 "</h5><p class=\"content\">" $4 "</p>"}'

echo "</div></div></div></body>"
