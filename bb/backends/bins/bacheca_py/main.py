import sqlite3
from jinja2 import Template
from jinja2 import Environment, FileSystemLoader
file_loader = FileSystemLoader('/home/celeste/bacheca_rust/templates')
env = Environment(loader=file_loader)

con = sqlite3.connect("/opt/bb/bb.db")
cur = con.cursor()

rows = []
for row in cur.execute('SELECT id, uname, time, content FROM posts ORDER BY time desc'):
    rows.append({
        "id": row[0],
        "username": row[1],
        "time": row[2],
        "content": row[3]
    })

temp = env.get_template("bb.html")

print(
    temp.render(
        total=len(rows),
        posts=rows,
        language="Python"
    )
)