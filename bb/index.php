<!DOCTYPE html>
<head>

		<link rel="stylesheet" href="/assets/css/skeleton.css">
		<link rel="stylesheet" href="/assets/css/nixphere.css">
		<link rel="stylesheet" href="/assets/css/normalize.css">
		<link rel="stylesheet" href="/assets/forkawesome/css/fork-awesome.min.css">
<style>
.title{
text-decoration: underline;
}
.name{
text-transform: capitalize;
}
.content{
max-width: 75%;
}
/*
.post{
margin: 1%;
padding: 1%;
border-radius: 10px;
}
*/
</style>
<title>bb</title>
</head>

<body>
<?php


$db = new SQLite3("/opt/bb/bb.db");

$res = $db->query("select count('1') from posts");
$res = $res->fetchArray();

echo '<div class="container">';
echo '<div class="row">';

echo '<div class="twelve columns">';
echo "<h1>bb: a bulletin board system written in many languages</h1>";
echo "<p> Total Posts: $res[0] </p>";
echo "<p>";
include("engines.php");
echo "</p>";
echo "<hr>";
echo "</div>";


$res = $db->query("select * from posts order by id desc");

while ($i = $res->fetchArray()){
	echo "<div class='twelve columns post'>";
	echo "<h5 class='title'>#{$i['id']} Posted by <span class='name'> {$i['uname']} </span> On {$i['time']}</h5>";

	echo "<p class='content'>{$i['content']}</p>";

	echo "</div>";
}

echo "</div>";
echo "</div>";
?>
</body>
