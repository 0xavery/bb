#!/usr/local/bin/tclsh8.6
load /usr/local/lib/sqlite3/libsqlite3.34.1.so Sqlite3
sqlite3 db /opt/bb/bb.db


puts "<!DOCTYPE html> <html><head><link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <title>TCL backend</title> </head> <body> <div class='container'><div class='row'> <div class='twelve columns'><h1>People still use TCL? </h1> <hr></div><div class='twelve columns'>\n"


db eval {SELECT * FROM posts ORDER BY ID desc} {
	puts "<h5 class='title'># $id Posted by $uname on $time </h5><p> $content </p>\n"
}

puts "</div></div></div></body>\n";

db close
