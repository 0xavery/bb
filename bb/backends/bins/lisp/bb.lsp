; no quicklisp for old apache
; no asdf for old apache
; no filesystem write for old apache
; no sqlite drivers for old lisp
; only csv parsing for old lisp

(write-line "<!DOCTYPE html>")
(write-line "<head> <link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <style> .title{ text-decoration: underline; } .name{ text-transform: capitalize; } .content{ max-width: 75; } </style> <title>bb</title> </head> <body> <div class='container'><div class='row'><div class='twelve columns'><h1>bb: a bulletin board system written in many languages</h1><p>This backend is writtein in <b> (((lisp))) </b><hr></div><div class='twelve columns post'><h5 class='post'>#")

; defvars
(setq file "/opt/bb/dump.csv")
(setq pipe 0)
(setq pipetrue 0)
(setq prerunlen 0)
(setq runlen 0)

; get file length loop
(with-open-file (stream file)
  (loop for c = (read-char stream nil)
	while c
	do

	(setq prerunlen (+ prerunlen 1))
	)
  )


; print file loop
(with-open-file (stream file)
  (loop for c = (read-char stream nil)
	while c
	do

	(setq runlen (+ runlen 1))

	; check if newline and not the last newline
	(if (and (eq c #\newline) (not (eq prerunlen runlen)))
	  ; progn lets us eval 0+ forms (ie run more than just one line per if-else
	  (progn 
	    (setq pipe 0)
	    (format t "</p>~%<h5 class='post'>#")
	    )
	  )

	; check if pipe
	(if (eq c #\|)
	  ; if
	  (progn
	    (setq pipe (+ pipe 1))
	    (setq pipetrue 1)
	    )
	  ; else 
	  (write-char c)
	  )

	; print format based on the pipe we're at
	(if (and (eq pipe 1) (eq pipetrue 1))
	  (write-line " Posted by ")
	  )

	(if (and (eq pipe 2) (eq pipetrue 1))
	  (write-line " on ")
	  )

	(if (and (eq pipe 3) (eq pipetrue 1))
	  (write-line "</h5><p>")
	  )


	; reset
	(setq pipetrue 0)
	)

  )

(write-line "</p></div></div></div></body>")
