#!/bin/sh
username=$(whoami)
tmpfile=$(mktemp /tmp/bb-$username.XXXXXX)

$EDITOR $tmpfile

content=$(cat $tmpfile | sed -e s/\"/\"\"/g -e s/\,/\\\,/g -e s/\;/\\\;/g -e s/\(/\\\(/g -e s/\)/\\\)/g )

# -e s/\n/\\\n/g) # maybe don't need to escape newlines?


#echo $content

echo INSERT INTO posts \(uname, content\) VALUES \(\"$username\", \"$content\"\)\; | sqlite3 /opt/bb/bb.db

maxid=$(echo "SELECT MAX(id) FROM posts;" | sqlite3 /opt/bb/bb.db)
newpost=$(echo "SELECT * FROM posts where id=$maxid;" | sqlite3 /opt/bb/bb.db)

echo newpost

rm $tmpfile

/opt/bb/updatecsv.sh
