#!/usr/local/bin/perl

use DBI;
use strict;

my $driver = "SQLite";
my $database = "/opt/bb/bb.db";
my $dsn = "DBI:$driver:dbname=$database";
my $dbh = DBI->connect($dsn, {RaiseError =>1})
	or die $DBI::errstr;


my $stmt = qq(select * from posts order by id desc;);
my $sth = $dbh->prepare($stmt);
my $rv = $sth->execute() or die $DBI::errstr;

print "<!DOCTYPE html> 
<html><head><link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <title>perl bb</title> </head> <body> <div class='container'><div class='row'> <div class='twelve columns'><h1>A maintainable perl backend. </h1> <hr></div><div class='twelve columns'>\n";

if($rv<0){
	print $DBI::errstr;
}

while(my @row = $sth->fetchrow_array()){
	print "<h5 class='title'>#" . $row[0] . " Posted by " .  $row[1] . " on " . $row[2] . "</h5><p>". $row[3] . "</p>\n";
}

print "</div></div></div></body>\n";
$dbh->disconnect();
