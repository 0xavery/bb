require 'sqlite3'

db = SQLite3::Database.open "/opt/bb/bb.db"

sql = db.prepare "select count('1') from posts;"
res = sql.execute

print "<!DOCTYPE html>\n"
print "<head> <link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <style> .title{ text-decoration: underline; } .name{ text-transform: capitalize; } .content{ max-width: 75%; } .post{ margin: 1%; padding: 1%; border-radius: 10px; } */ </style> <title>bb</title> </head> <body> <div class='container'><div class='row'><div class='twelve columns'><h1>bb: a bulletin board system written in many languages</h1><p> This backend is written in <b>Ruby</b></p><p> Total Posts: "


res.each do |row|
  print row.join "\s"
end

print "</p><hr></div><div class='twelve columns post'>"


sql = db.prepare "select * from posts order by id desc;"
res = sql.execute

res.each_hash do |row|
  # ["id", "uname", "time", "content"]
  print "<h5 class='title'>#"
  print row['id']
  print " Posted By <b>"
  print row['uname']
  print "</b> on "
  print row['time']
  print "</h5><p>"
  print row['content']
  print "</p>"
end

print "</div></div></div></body>"
