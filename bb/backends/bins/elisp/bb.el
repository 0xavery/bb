(setq file "/opt/bb/dump.csv")

(setq buf (with-temp-buffer
	    (insert-file-contents file)
	    (split-string (buffer-string) "\n" t)))

(setq delim 1)
(setq newloop 1)

(princ "<!DOCTYPE html>")
(princ "<head> <link rel='stylesheet' href='/assets/css/skeleton.css'> <link rel='stylesheet' href='/assets/css/nixphere.css'> <link rel='stylesheet' href='/assets/css/normalize.css'> <link rel='stylesheet' href='/assets/forkawesome/css/fork-awesome.min.css'> <style> .title{ text-decoration: underline; } .name{ text-transform: capitalize; } .content{ max-width: 75; } </style> <title>bb</title> </head> <body> <div class='container'><div class='row'><div class='twelve columns'><h1>bb: a bulletin board system written in many languages</h1><p>This backend is writtein in <b> (((emacs lisp))) </b><hr></div><div class='twelve columns post'>")

(princ "<h5 class='post'>#")

(dolist (n buf)
  (progn
					;(setq i (+ i 1))
					;(print i)
					;(print n)
    (setq tmpstr (split-string n "|" t))
    (dolist (o tmpstr)
      (progn

	(setq newloop 1)
	;(if (= delim 4)
	;    (progn
	;      (setq delim 1)
	;      (princ (format "<h5 class='post'>#%s" o ))
	;      )
	;  )

	(if (= delim 4)
	    (progn
	      (setq delim 1)
	      (setq newloop 0)
	      (princ (format " </h5><p>%s</p>\n<h5 class='post'>#" o))
	      )
	  )

	(if (= delim 3)
	    (progn
	      (setq newloop 0)
	      (setq delim (+ delim 1))
	      (princ (format " on %s" o))
	      )
	  )

	(if (= delim 2)
	    (progn
	      (setq newloop 0)
	      (setq delim (+ delim 1))
	      (princ (format " posted by %s" o))
	      )
	  )
	
	(if (and (= delim 1) (= newloop 1))
	    (progn
	      (setq delim (+ delim 1))
	      (princ (format "%s" o))
	      )
	  )




	)
      )
    )
  )
(princ "</h5></div></div></div></body>")
(terpri)
