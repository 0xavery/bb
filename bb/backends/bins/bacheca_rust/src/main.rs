use askama::Template;
//use sqlite::Value;

#[derive(Template)]
#[template(path = "bb.html")]
struct BBTemplate {
    total: i32,
    posts: Vec<Post>,
    language: String
}

struct Post{
    id: i32,
    username: String,
    time: String,
    content: String
}

fn main() {
    // Test section
/*
    let connection = sqlite::open(":memory:").unwrap();
    connection.execute(
        "
        CREATE TABLE posts(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            uname TEXT NOT NULL,
            time DATETIME DEFAULT current_timestamp,
            content TEXT NOT NULL
        );
        INSERT INTO posts VALUES(1,'avery','2021-08-14 08:21:45','This is a test post. Hopefully the first of many. I can''t yet decide if I should tape this to apache and PHP or just call a modified version of \"Getposts\" from apache. Either way, hopefully this works. ');
        ",
    ).unwrap();
    // End test
*/
    let connection = sqlite::open("/opt/bb/bb.db").unwrap();

    let mut cursor = connection.prepare("SELECT id, uname, time, content FROM posts ORDER BY time desc")
    .unwrap()
    .into_cursor();

    let mut posts: Vec<Post> = Vec::new();

    while let Some(row) = &cursor.next().unwrap() {
        posts.push(
            Post{
                id: row[0].as_integer().unwrap() as i32,
                username: row[1].as_string().unwrap().to_string(),
                time: row[2].as_string().unwrap().to_string(),
                content: row[3].as_string().unwrap().to_string(),
            }
        );
    }

    let hello = BBTemplate{
        total: posts.len() as i32,
        posts:  posts,
        language: "Rust".to_string()
    };
    println!("{}", hello.render().unwrap()); 
}
